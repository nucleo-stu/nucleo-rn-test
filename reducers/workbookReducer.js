import { LOAD_WORKBOOK_STAGE, LOAD_WORKBOOK_STAGE_SUCCESS, UPDATE_WORKBOOK_FIELD, UPDATE_WORKBOOK_FIELD_SUCCESS, WORKBOOK_ERROR } from "../lib/types"

const INITIAL_STATE = {
	isLoading: false,
	isLoaded: false,
	data: {
		currentStageNumber: 0,
		currentStageProgress: 0,
		currentStageFields: [],
	},
	error: null,
}

const getProgress = (fields) => {
	return fields.filter((field) => field.complete !== 0).length / fields.length
}

const workbookReducer = (state = INITIAL_STATE, action) => {
	console.log(action.type)
	switch (action.type) {
		case LOAD_WORKBOOK_STAGE:
			return {
				...state,
				isLoading: true,
			}
		case LOAD_WORKBOOK_STAGE_SUCCESS:
			return {
				...state,
				isLoaded: true,
				isLoading: false,
				data: {
					currentStageNumber: action.data.stageNumber,
					currentStageFields: action.data.stageFields,
					currentStageProgress: getProgress(action.data.stageFields),
				},
			}
		case UPDATE_WORKBOOK_FIELD_SUCCESS:
			const newStageFields = state.data.currentStageFields.map((field) => {
				if (field.fieldKey === action.data.fieldKey) field.content = action.data.value
				return field
			})

			return {
				...state,
				hasProfile: false,
				isLoaded: true,
				data: {
					...state.data,
					currentStageFields: newStageFields,
					currentStageProgress: getProgress(newStageFields),
				},
			}
		case WORKBOOK_ERROR:
			return {
				...INITIAL_STATE,
				error: action.err,
			}
		default:
			return state
	}
}

export default workbookReducer
