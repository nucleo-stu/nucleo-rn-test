import { combineReducers } from "redux"
import profileReducer from "./profileReducer"
import workbookReducer from "./workbookReducer"

export default combineReducers({
	profile: profileReducer,
	workbook: workbookReducer,
})
