import { LOAD_PROFILE, LOAD_PROFILE_NOT_CREATED, LOAD_PROFILE_STATS_SUCCESS, LOAD_PROFILE_SUCCESS, PROFILE_ERROR } from "../lib/types"

const INITIAL_STATE = {
	isLoaded: false,
	isLoading: false,
	data: {
		currentProfileGroup: "",
		currentProfileFields: [],
	},
	stats: {},
	err: null,
}

const profileReducer = (state = INITIAL_STATE, action) => {
	console.log(action.type)
	switch (action.type) {
		case LOAD_PROFILE:
			return {
				...state,
				isLoading: true,
				isLoaded: false,
			}
		case LOAD_PROFILE_SUCCESS:
			return {
				...state,
				data: {
					currentProfileGroup: action.data.group,
					currentProfileFields: action.data.fields,
				},
				isLoading: false,
				isLoaded: true,
			}
		case LOAD_PROFILE_NOT_CREATED:
			return {
				...state,
				data: {
					...INITIAL_STATE.data,
				},
				isLoading: false,
				isLoaded: false,
				error: action.err,
			}
		case LOAD_PROFILE_STATS_SUCCESS:
			return {
				...state,
				stats: action.stats,
			}
		case PROFILE_ERROR:
			return {
				isLoading: false,
				isLoaded: false,
				error: action.err,
			}
		default:
			return state
	}
}

export default profileReducer
