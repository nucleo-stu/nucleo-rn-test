import { LOAD_WORKBOOK_STAGE, LOAD_WORKBOOK_STAGE_SUCCESS, UPDATE_WORKBOOK_FIELD, UPDATE_WORKBOOK_FIELD_SUCCESS, WORKBOOK_ERROR } from "../lib/types"
import Database from "../lib/db"

const db = Database.connect()

// export const createEmptyWorkbookStage = () => (dispatch, getState) => {
//
//     dispatch({
//         type: CREATE_PROFILE
//     })
//
//     db.transaction(
//         tx => {
//             tx.executeSql(`insert into workbook (firstName, lastName, email, mobile)
//                            values (?, ?, ?, ?)`, ["", "", "", ""])
//         },
//         onError,
//         () => {
//             dispatch({
//                 type: CREATE_PROFILE_SUCCESS
//             })
//             dispatch(loadProfile())
//         }
//     )
// }

export const loadStage = (stageNumber) => (dispatch, getState) => {
	console.log("Loading stage: ", stageNumber)

	dispatch({
		type: LOAD_WORKBOOK_STAGE,
	})

	db.transaction((tx) => {
		tx.executeSql("select * from workbook where stage = ?;", [stageNumber], (_, { rows }) => {
			// tx.executeSql("select * from workbook;", [], (_, {rows}) => {

			if (rows._array.length > 0) {
				const stageData = rows._array

				dispatch({
					type: LOAD_WORKBOOK_STAGE_SUCCESS,
					data: {
						stageNumber: stageNumber,
						stageFields: stageData,
					},
				})
			} else {
				dispatch({
					type: WORKBOOK_ERROR,
					err: "Could not load the requested stage, or stage empty",
				})
			}
		})
	}, onError)
}

export const updateStageField = (fieldKey, value) => (dispatch, getState) => {
	dispatch({
		type: UPDATE_WORKBOOK_FIELD,
	})

	console.log(fieldKey, value)

	db.transaction(
		(tx) => {
			tx.executeSql(
				`update workbook
                     set content  = ?,
                         complete = ?
                     where fieldKey = ?;`,
				[value, value.length, fieldKey]
			)
		},
		onError,
		() => {
			dispatch({
				type: UPDATE_WORKBOOK_FIELD_SUCCESS,
				data: {
					fieldKey,
					value,
				},
			})
			// dispatch(loadProfile())
		}
	)
}

const onError = (err) => {
	console.log(err)
}
