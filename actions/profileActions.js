import {
	CREATE_PROFILE,
	CREATE_PROFILE_SUCCESS,
	DELETE_PROFILE,
	DELETE_PROFILE_SUCCESS,
	LOAD_PROFILE,
	LOAD_PROFILE_NOT_CREATED,
	LOAD_PROFILE_SUCCESS,
	UPDATE_PROFILE,
	UPDATE_PROFILE_SUCCESS,
} from "../lib/types"
import Database from "../lib/db"

import { fieldKeys } from "../lib/migrations/profile"

const db = Database.connect()

export const loadProfileGroup = (groupKey) => (dispatch, getState) => {
	dispatch({
		type: LOAD_PROFILE,
	})

	db.transaction((tx) => {
		tx.executeSql("select * from profile where groupKey = ?;", [groupKey], (_, { rows }) => {
			if (rows._array.length > 0) {
				const profileData = rows._array

				dispatch({
					type: LOAD_PROFILE_SUCCESS,
					data: {
						group: groupKey,
						fields: profileData,
					},
				})

				console.log(profileData)
			} else {
				dispatch({
					type: LOAD_PROFILE_NOT_CREATED,
					err: "Likely an error in the migration? Fields not found",
				})
			}
		})
	}, onError)
}

export const updateProfileField = (fieldKey, value) => (dispatch, getState) => {
	dispatch({
		type: UPDATE_PROFILE,
	})

	db.transaction(
		(tx) => {
			tx.executeSql(
				`update profile
                           set content = ?
                           where fieldKey = ?;`,
				[value, fieldKey]
			)
		},
		onError,
		() => {
			dispatch({
				type: UPDATE_PROFILE_SUCCESS,
			})
		}
	)
}

export const loadProfileCompletion = () => (dispatch, getState) => {
	db.transaction(
		(tx) => {
			Object.keys(fieldKeys).forEach((groupKey) => {
				tx.executeSql(`select * from profile where groupKey = ?;`, [groupKey], (_, { rows }) => {
					console.log(rows._array)
				})
			})
		},
		onError,
		() => {
			dispatch({
				type: UPDATE_PROFILE_SUCCESS,
			})
		}
	)
}

export const loadProfileCompletionByGroup = (groupKey) => (dispatch, getState) => {
	db.transaction(
		(tx) => {
			tx.executeSql(`select count(*) from profile where  groupKey = ?;`, [true], (_, { rows }) => {
				console.log(rows._array)
			})
		},
		onError,
		() => {
			dispatch({
				type: UPDATE_PROFILE_SUCCESS,
			})
		}
	)
}

const onError = (err) => {
	console.log(err)
}
