const StageOneFieldKeys = ["s1_mobms", "s1_comm", "s1_socin", "s1_learn", "s1_selfcare", "s1_selfmanag"]

const AllFieldKeys = [...StageOneFieldKeys]

export const workbookMigrations = (db) => {
	return new Promise((resolve, reject) => {
		db.transaction(
			(tx) => {
				tx.executeSql("select * from workbook;", [], (_, { rows }) => {
					if (rows._array.length > 0) {
						console.log("Workbook fields found: ", rows._array.length)

						const existingWorkbookFieldKeys = rows._array.map((field) => field.fieldKey)
						const missingFields = AllFieldKeys.filter((key) => !existingWorkbookFieldKeys.includes(key))

						if (missingFields.length > 0) {
							console.log("Missing fields: ", missingFields.length)
							missingFields.forEach((key) => {
								tx.executeSql(
									`insert into workbook (stage, fieldKey, content, complete)
                                       values (?, ?, ?, ?)`,
									[1, key, "", 0],
									() => {
										console.log("inserted empty field = ", key)
									}
								)
							})
						} else {
							console.log("All fields exist, no migration to run")
						}
					} else {
						console.log("No workbook fields found")
						StageOneFieldKeys.forEach((key) => {
							tx.executeSql(
								`insert into workbook (stage, fieldKey, content, complete)
                                       values (?, ?, ?, ?)`,
								[1, key, "", 0],
								() => {
									console.log("Inserted empty field = ", key)
								}
							)
						})
					}
				})
			},
			reject,
			() => {
				console.log("Workbook migration complete")
				resolve()
			}
		)
	})
}
