export const fieldKeys = {
	personalDetails: ["pd_name", "pd_birthday", "pd_address", "pd_phone", "pd_email", "pd_bio"],
}

export const profileMigrations = (db) => {
	return new Promise((resolve, reject) => {
		db.transaction((tx) => {
			tx.executeSql(
				"select * from profile;",
				[],
				(_, { rows }) => {
					if (rows._array.length > 0) {
						console.log("All fields exist, no migration to run")
					} else {
						console.log("No Profile fields found")

						Object.keys(fieldKeys).forEach((groupKey) => {
							fieldKeys[groupKey].forEach((fieldKey) => {
								tx.executeSql(`insert into profile (groupKey, fieldKey, content, complete) values (?, ?, ?, ?)`, [groupKey, fieldKey, "", 0])
							})
						})
					}
				},
				reject,

				() => {
					console.log("Profile migration complete")
					resolve()
				}
			)
		})
	})
}
