
export const BrandPurple = "#98459A"
export const BrandPurpleLight = "#b887bc"
export const BrandTeal = "#27a891"
export const BrandTealLight = "#8cc4b6"
export const BrandOrange = "#f8991d"
export const BrandOrangeLight = "#fed29f"
export const BrandOlive = "#81c67a"
export const BrandOliveLight = "#b3daaa"
export const BrandSecondaryPink = "#e55682"
export const BrandSecondaryRed = "#c40000"
export const BrandSecondaryBlue = "#6251d5"

export const LightGrey = "#eeeeee"
export const DarkGrey = "#707070"

export const TextColour = DarkGrey;
export const HeadingColourPrimary = BrandTeal;
export const HeadingColourSecondary = BrandPurple;
