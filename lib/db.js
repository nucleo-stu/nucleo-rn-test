import * as SQLite from "expo-sqlite"
import { workbookMigrations } from "./migrations/workbook"
import { profileMigrations } from "./migrations/profile"

const db = SQLite.openDatabase("db.db")

class Database {
	static connect() {
		return db
	}

	static createTables() {
		return new Promise((resolve, reject) => {
			db.transaction(
				(tx) => {
					// tx.executeSql("drop table if exists profile;")
					// tx.executeSql("drop table if exists workbook;")
					tx.executeSql(
						"create table if not exists profile (id integer primary key not null, groupKey text, fieldKey text, content text, complete integer);"
					)
					tx.executeSql(
						"create table if not exists workbook (id integer primary key not null, stage integer, fieldKey text, content text, complete integer);"
					)
				},
				reject,
				resolve
			)
		})
	}

	static runMigrations() {
		return Promise.all([workbookMigrations(db), profileMigrations(db)])
	}
}

export default Database
