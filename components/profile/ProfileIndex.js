import React, { Component } from "react"
import { TouchableOpacity, View, Text } from "react-native"
import { connect } from "react-redux"
import { Container, Wrapper } from "../Layout"
import { HeadingTwo } from "../Typography"
import { Card } from "react-native-paper"
import { FontAwesome } from "@expo/vector-icons"
import { loadProfileCompletion } from "../../actions/profileActions"

const ProgressIcon = (props) => {
	return <FontAwesome name={"check"} />
}

class ProfileIndex extends Component {
	componentDidMount() {
		const { loadProfileCompletion } = this.props
		loadProfileCompletion()
	}

	render() {
		const { navigation } = this.props
		return (
			<Wrapper>
				<Container>
					<HeadingTwo>Profile</HeadingTwo>
				</Container>
				{/*<Text>{JSON.stringify(this.props.stats)}</Text>*/}
				<TouchableOpacity
					onPress={() => {
						navigation.navigate("Personal Details")
					}}
				>
					<Card.Title title={"Personal details"} right={(props) => <ProgressIcon {...props} />} rightStyle={{ paddingRight: 20 }} />
				</TouchableOpacity>
			</Wrapper>
		)
	}
}

function mapStateToProps(state) {
	return {
		stats: state.profile.stats,
	}
}

export default connect(mapStateToProps, {
	loadProfileCompletion,
})(ProfileIndex)
