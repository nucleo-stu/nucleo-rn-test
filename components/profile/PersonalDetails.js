import React, { Component } from "react"
import { connect } from "react-redux"
import { updateProfileField, loadProfileGroup } from "../../actions/profileActions"
import { ScrollView, StyleSheet, View, Text, SafeAreaView } from "react-native"
import ProfileDetails from "../ProfileDetails"
import ProfileForm from "../ProfileForm"
import { Center, Container } from "../Layout"
import { Content, HeadingThree } from "../Typography"
import { DatePickerInput, TextBoxInput } from "../FormElements"

class PersonalDetails extends Component {
	componentDidMount() {
		const { loadProfileGroup } = this.props
		loadProfileGroup("personalDetails")
	}

	componentDidUpdate(prevProps, prevState, snapshot) {}

	handleUpdateField = (name) => (value) => {
		const { updateProfileField } = this.props
		updateProfileField(name, value)
	}

	getInitialFieldValue = (key) => {
		const { fields } = this.props

		return fields.find((field) => field.fieldKey === key).content
	}

	render() {
		const { isLoaded, fields } = this.props

		return (
			<View style={styles.container}>
				<SafeAreaView style={styles.container}>
					<ScrollView style={styles.scrollView}>
						<Container>
							{/*<Text>{JSON.stringify(fields)}</Text>*/}
							{isLoaded && (
								<View>
									<View>
										<Center>
											<HeadingThree center>Name</HeadingThree>
										</Center>
										<TextBoxInput
											placeholder={"Fill in your name here"}
											initialValue={this.getInitialFieldValue("pd_name")}
											handleSave={this.handleUpdateField("pd_name")}
										/>
									</View>
									<View>
										<Center>
											<HeadingThree center>Birthday</HeadingThree>
										</Center>
										<DatePickerInput
											placeholder={"Fill in your answer here"}
											initialValue={this.getInitialFieldValue("pd_birthday")}
											handleSave={this.handleUpdateField("pd_birthday")}
										/>
									</View>
									<View>
										<Center>
											<HeadingThree center>Address</HeadingThree>
										</Center>
										<TextBoxInput
											placeholder={"Fill in your answer here"}
											initialValue={this.getInitialFieldValue("pd_address")}
											handleSave={this.handleUpdateField("pd_address")}
										/>
									</View>
									<View>
										<Center>
											<HeadingThree center>Phone</HeadingThree>
										</Center>
										<TextBoxInput
											placeholder={"Fill in your answer here"}
											initialValue={this.getInitialFieldValue("pd_phone")}
											handleSave={this.handleUpdateField("pd_phone")}
										/>
									</View>
									<View>
										<Center>
											<HeadingThree center>Email</HeadingThree>
										</Center>
										<TextBoxInput
											placeholder={"Fill in your answer here"}
											initialValue={this.getInitialFieldValue("pd_email")}
											handleSave={this.handleUpdateField("pd_email")}
										/>
									</View>
									<View>
										<Center>
											<HeadingThree center>Bio</HeadingThree>
										</Center>
										<TextBoxInput
											placeholder={"Fill in your answer here"}
											initialValue={this.getInitialFieldValue("pd_bio")}
											handleSave={this.handleUpdateField("pd_bio")}
										/>
									</View>
								</View>
							)}
						</Container>
					</ScrollView>
				</SafeAreaView>
			</View>
		)
	}
}

function mapStateToProps(state) {
	return {
		fields: state.profile.data.currentProfileFields,
		isLoaded: state.profile.isLoaded,
	}
}

export default connect(mapStateToProps, {
	loadProfileGroup,
	updateProfileField,
})(PersonalDetails)

const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: "#fff",
	},
})
