import React, { Component, useState } from "react"
import { StyleSheet, Text, TextInput, TouchableOpacity, View, Button, Platform } from "react-native"
import { MaterialIcons } from "@expo/vector-icons"
import * as constants from "../lib/constants"
import DateTimePicker from "@react-native-community/datetimepicker"
import format from "date-fns/format"
import { onChange } from "react-native-reanimated"

const SaveIcon = ({ focus, value }) => {
	const handlePress = (e) => {
		console.log("PRESSED TICK")
	}

	if (value.length > 0 && !focus) {
		return (
			<TouchableOpacity style={styles.checkIconWrap}>
				<MaterialIcons style={styles.checkIcon} name={"check"} size={20} />
			</TouchableOpacity>
		)
	} else return null
}

const CancelIcon = ({ focus, value, onPress }) => {
	const handlePress = (e) => {
		console.log("CANCEL PRESS")
		onPress()
	}

	if (value.length > 0) {
		return focus ? (
			<TouchableOpacity style={styles.cancelIconWrap} onPress={handlePress}>
				<MaterialIcons style={styles.cancelIcon} name={"close"} size={20} />
			</TouchableOpacity>
		) : null
	} else return null
}

export const TextBoxInput = ({ initialValue, placeholder, handleSave }) => {
	const [value, onChangeText] = useState(initialValue || "")
	const [focus, setFocus] = useState(false)
	const [wrapFocus, setWrapFocus] = useState(false)
	const [isCancelling, setIsCancelling] = useState(false)

	const inputStyles = () => {
		if (focus) {
			return styles.inputWrapFocus
		} else if (value === "") {
			return styles.inputWrapEmpty
		} else {
			return styles.inputWrap
		}
	}

	const handleBlur = () => {
		saveField()
	}

	const saveField = () => {
		if (value !== initialValue) {
			handleSave(value)
		}
		setFocus(false)
	}

	return (
		<View style={inputStyles()} onFocus={() => setWrapFocus(true)} onBlur={() => setWrapFocus(false)}>
			{/*<Text>{JSON.stringify(wrapFocus)}</Text>*/}
			<TextInput
				style={styles.input}
				value={value}
				multiline
				onFocus={() => setFocus(true)}
				onBlur={handleBlur}
				onChangeText={onChangeText}
				placeholder={placeholder}
			/>
			<SaveIcon value={value} focus={focus} />

			{/*<CancelIcon value={value} focus={focus} onPress={revertChanges} />*/}
			{/*{!focus && value.length > 0 && <MaterialIcons name={"check"} style={styles.checkIcon} size={20} />}*/}
		</View>
	)
}

export const DatePickerInput = ({ initialValue, placeholder, handleSave }) => {
	const [value, setValue] = useState(initialValue || "")
	const [focus, setFocus] = useState(false)

	const [date, setDate] = useState(new Date(1598051730000))
	const [mode, setMode] = useState("date")
	const [show, setShow] = useState(false)

	const inputStyles = () => {
		if (focus) {
			return styles.inputWrapFocus
		} else if (value === "") {
			return styles.inputWrapEmpty
		} else {
			return styles.inputWrap
		}
	}

	const handleBlur = () => {
		handleSave(value)
		setFocus(false)
	}

	const onChange = (event, selectedDate) => {
		const currentDate = selectedDate || date
		setShow(Platform.OS === "ios")
		setDate(currentDate)
		setValue(format(currentDate, "d MMM Y"))
	}
	const showMode = (currentMode) => {
		setShow(true)
		setMode(currentMode)
	}

	const showDatepicker = () => {
		setFocus(true)
		showMode("date")
	}

	return (
		<View style={inputStyles()}>
			<TextInput
				style={styles.input}
				value={value}
				multiline
				onFocus={showDatepicker}
				onBlur={handleBlur}
				onChangeText={setValue}
				placeholder={placeholder}
			/>
			<SaveIcon value={value} focus={focus} />

			{show && (
				<DateTimePicker
					testID="dateTimePicker"
					timeZoneOffsetInMinutes={0}
					value={date}
					mode={mode}
					is24Hour={true}
					display="default"
					onChange={onChange}
				/>
			)}
		</View>
	)
}

const inputWrapStyles = {
	borderWidth: 1,
	borderStyle: "solid",
	borderRadius: 30,
	borderColor: constants.BrandTealLight,
	paddingVertical: 10,
	marginVertical: 10,
}

const styles = StyleSheet.create({
	inputWrap: inputWrapStyles,
	inputWrapFocus: {
		...inputWrapStyles,
		borderColor: constants.BrandSecondaryPink,
	},
	inputWrapEmpty: {
		...inputWrapStyles,
		borderColor: constants.BrandOrangeLight,
	},
	input: {
		width: "70%",
		alignSelf: "center",
	},
	checkIconWrap: {
		position: "absolute",
		top: 13,
		right: 15,
		width: 20,
		height: 20,
		color: constants.BrandTealLight,
	},
	cancelIconWrap: {
		position: "absolute",
		top: 13,
		left: 15,
		width: 20,
		height: 20,
		color: constants.BrandTealLight,
		zIndex: 2,
	},
	checkIcon: {
		position: "absolute",
		top: 0,
		right: 0,
		color: constants.BrandTealLight,
	},
	cancelIcon: {
		position: "absolute",
		top: 0,
		left: 0,
		color: constants.BrandSecondaryRed,
	},
})
