import React from 'react';
import {StyleSheet, Text, View} from 'react-native';

const ProfileDetails = ({ profile }) => {
  
  const { firstName, lastName, email, mobile } = profile
  
  return (
    <View style={styles.container}>
      <Text style={styles.heading}>Profile details:</Text>
      <Text>(loaded from SQLite)</Text>
      <View>
        <View style={styles.detailRow}><Text style={styles.detailLabel}>First Name:</Text><Text style={styles.detailLabel}>{firstName || "Not Entered"}</Text></View>
        <View style={styles.detailRow}><Text style={styles.detailLabel}>Last Name:</Text><Text style={styles.detailLabel}>{lastName || "Not Entered"}</Text></View>
        <View style={styles.detailRow}><Text style={styles.detailLabel}>Email:</Text><Text style={styles.detailLabel}>{email || "Not Entered"}</Text></View>
        <View style={styles.detailRow}><Text style={styles.detailLabel}>Mobile:</Text><Text style={styles.detailLabel}>{mobile || "Not Entered"}</Text></View>
      </View>
    </View>
  );
}

export default ProfileDetails

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingVertical: 30,
    paddingHorizontal: 30,
    backgroundColor: "#fbfbfb",
  },
  heading : {
    fontSize: 20,
    fontWeight: "400",
    paddingBottom: 15,
  },
  detailRow: {
    flexDirection: "row",
    paddingVertical: 5,
  },
  detailLabel: {
    flex: 1,
  }
})