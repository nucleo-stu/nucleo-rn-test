import React, { Component } from "react"
import { connect } from "react-redux"
import { View, Text, StyleSheet, Image } from "react-native"
import Swiper from "react-native-swiper"
import * as constants from "../lib/constants"
import { Wrapper } from "./Layout"
import { ButtonRev, HeadingOne } from "./Typography"
import { Button } from "react-native-paper"
import { blue50 } from "react-native-paper/src/styles/colors"
import { StepImage } from "./workbook/StepImage"

const logoRev = require("../assets/logos/logo-rev.png")
const slide1ImgSrc = require("../assets/home-graphics/home-2.jpg")
const slide2ImgSrc = require("../assets/home-graphics/home-3.jpg")
const slide3ImgSrc = require("../assets/home-graphics/home-4.jpg")

export const SlideImage = ({ source, ...props }) => {
	return (
		<View style={styles.imageWrap}>
			<Image style={styles.image} source={source} resizeMode={"cover"} {...props} />
		</View>
	)
}

export const LogoImage = ({ source, ...props }) => {
	return (
		<View style={styles.logoImageWrap}>
			<Image style={styles.logoImage} source={source} resizeMode={"contain"} {...props} />
		</View>
	)
}

class InitScreen extends Component {
	render() {
		return (
			<View style={{ flex: 1 }}>
				<Swiper style={styles.wrapper} showsButtons={false} dotStyle={styles.dot} activeDotStyle={styles.activeDot}>
					<View style={styles.slide}>
						<SlideImage source={slide1ImgSrc} />
						<View style={styles.textWrap}>
							<HeadingOne>A tool to support you!</HeadingOne>
						</View>
					</View>
					<View style={styles.slide}>
						<SlideImage source={slide2ImgSrc} />
						<View style={styles.textWrap}>
							<HeadingOne>An app for you,{"\n"} about you.</HeadingOne>
						</View>
					</View>
					<View style={styles.slide}>
						<SlideImage source={slide3ImgSrc} />
						<View style={styles.textWrap}>
							<HeadingOne>Explore who you are.</HeadingOne>
						</View>
					</View>
					<View style={styles.slide}>
						<SlideImage source={slide3ImgSrc} />
						<View style={styles.textWrap}>
							<HeadingOne>Reimagine your life.</HeadingOne>
						</View>
					</View>
				</Swiper>
				<View style={styles.logoButtonWrap}>
					<View style={{ flex: 1, justifyContent: "center", alignItems: "center" }}>
						<LogoImage source={logoRev} />
						<ButtonRev
							onPress={() => {
								this.props.navigation.navigate("Home")
							}}
						>
							Get Started!
						</ButtonRev>
					</View>
				</View>
				<View style={styles.footerWrap}>
					<Text>Funded by the ndis</Text>
				</View>
			</View>
		)
	}
}

const dotSize = 10

const styles = StyleSheet.create({
	wrapper: {
		height: "100%",
	},
	dot: {
		backgroundColor: "transparent",
		width: dotSize,
		height: dotSize,
		borderRadius: dotSize / 2,
		borderColor: "#fff",
		borderStyle: "solid",
		borderWidth: 1,
		marginLeft: 3,
		marginRight: 3,
		marginTop: 3,
		marginBottom: 3,
	},
	activeDot: {
		backgroundColor: "#FFF",
		width: dotSize,
		height: dotSize,
		borderRadius: dotSize / 2,
		marginLeft: 3,
		marginRight: 3,
		marginTop: 3,
		marginBottom: 3,
	},
	slide: {
		flex: 1,
		justifyContent: "center",
		alignItems: "center",
		backgroundColor: constants.BrandTeal,
	},
	text: {
		color: "#fff",
		// fontSize: 30,
		// fontWeight: "bold",
	},
	textWrap: {
		flex: 1,
		justifyContent: "center",
		alignItems: "center",
	},
	imageWrap: {
		flex: 1,
		justifyContent: "center",
		flexDirection: "row",
		backgroundColor: "pink",
		height: "50%",
	},
	image: {
		height: "100%",
		width: "100%",
	},
	logoButtonWrap: {
		position: "absolute",
		top: 0,
		left: 0,
		width: "100%",
		height: "50%",
		flex: 1,
		zIndex: 1,
		paddingVertical: 20,
	},
	logoImageWrap: {
		justifyContent: "center",
		paddingVertical: 30,
		flexDirection: "row",
		marginBottom: 20,
	},
	logoImage: {
		height: 120,
		width: 120,
	},
	footerWrap: {
		backgroundColor: constants.BrandPurple,
		padding: 20,
	},
})

function mapStateToProps(state) {
	return {}
}

export default connect(mapStateToProps)(InitScreen)
