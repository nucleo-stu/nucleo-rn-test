import React from "react"
import { createStackNavigator } from "@react-navigation/stack"
import { Button, View, TouchableOpacity } from "react-native"
import StepOne from "./workbook/StepOne"
import { Container } from "./Layout"
import { HeadingFive, HeadingFour, HeadingThree, HeadingTwo } from "./Typography"

const Stack = createStackNavigator()

const WorkbookStageLink = (props) => {
	return (
		<TouchableOpacity onPress={props.onPress}>
			<HeadingThree>{props.title}</HeadingThree>
			<HeadingFive>{props.subtitle}</HeadingFive>
		</TouchableOpacity>
	)
}

const WorkbookIndex = ({ navigation }) => {
	return (
		<Container>
			<HeadingTwo>Workbook</HeadingTwo>

			<WorkbookStageLink
				title={"Stage 1"}
				subtitle={"Understand psychosocial disability"}
				onPress={() => {
					navigation.navigate("Step 1")
				}}
			/>
		</Container>
	)
}

export const WorkbookStack = () => {
	return (
		<Stack.Navigator initialRouteName={"Workbook"}>
			<Stack.Screen name={"Workbook"} component={WorkbookIndex} />
			<Stack.Screen name={"Step 1"} component={StepOne} />
		</Stack.Navigator>
	)
}
