import React from "react"
import { Button, View, Text, StyleSheet, TouchableOpacity } from "react-native"
import * as constants from "../lib/constants"
import { BrandPurple } from "../lib/constants"

import { EvilIcons } from "@expo/vector-icons"
import { Linking } from "expo"

function getStyles(type, props) {
	const stylesArr = [styles[type]]

	if (props.center) stylesArr.push(styles.center)
	if (props.rev) stylesArr.push(styles.textRev)

	return StyleSheet.flatten(stylesArr)
}

export const HeadingOne = ({ children, ...props }) => {
	return <Text style={getStyles("h1", props)}>{children}</Text>
}

export const HeadingTwo = ({ children, ...props }) => {
	return <Text style={getStyles("h2", props)}>{children}</Text>
}

export const HeadingThree = ({ children, ...props }) => {
	return <Text style={getStyles("h3", props)}>{children}</Text>
}

export const HeadingFour = ({ children, ...props }) => {
	return <Text style={getStyles("h4", props)}>{children}</Text>
}

export const HeadingFive = ({ children, ...props }) => {
	return <Text style={getStyles("h5", props)}>{children}</Text>
}

export const Content = ({ children, ...props }) => {
	return <Text style={getStyles("content", props)}>{children}</Text>
}

export const TextLink = ({ children, href, ...props }) => {
	const handlePress = () => {
		Linking.openURL(href)
		props.onPress && props.onPress()
	}

	return (
		<TouchableOpacity style={styles.textLink} onPress={handlePress}>
			<EvilIcons name={"external-link"} color={constants.BrandPurple} size={20} />
			<Text style={styles.textLinkText}>{children}</Text>
		</TouchableOpacity>
	)
}

export const ButtonRev = ({ children, onPress, navigation, ...props }) => {
	return (
		<TouchableOpacity style={styles.buttonRev} onPress={onPress} activeOpacity={0.9}>
			<Text style={styles.buttonRevText}>{children}</Text>
		</TouchableOpacity>
	)
}

const buttonBaseStyles = {
	paddingVertical: 15,
	fontWeight: "bold",
	textTransform: "uppercase",
	paddingHorizontal: 40,
	minWidth: 200,
	borderRadius: 40,
}

const styles = StyleSheet.create({
	h1: {
		fontSize: 35,
		color: constants.BrandTealLight,
		marginBottom: 15,
	},
	h2: {
		fontSize: 26,
		color: constants.BrandPurple,
		marginBottom: 15,
		fontWeight: "bold",
	},
	h3: {
		fontSize: 20,
		color: constants.BrandPurple,
		marginBottom: 10,
		fontWeight: "bold",
	},
	h4: {
		fontSize: 18,
		color: constants.TextColour,
		// marginBottom: 10,
		fontWeight: "bold",
	},
	h5: {
		fontSize: 14,
		color: constants.TextColour,
		// marginBottom: 10,
		fontWeight: "bold",
	},
	content: {
		fontSize: 18,
		color: constants.TextColour,
		// marginBottom: 10,
		fontWeight: "normal",
	},
	textLink: {
		flex: 1,
		flexDirection: "row",
		marginVertical: 30,
	},
	textLinkText: {
		color: constants.BrandPurple,
		fontWeight: "bold",
	},
	center: {
		textAlign: "center",
	},
	textRev: {
		color: "#fff",
	},
	buttonRev: {
		...buttonBaseStyles,
		backgroundColor: "#fff",
	},
	buttonRevText: {
		textAlign: "center",
		fontWeight: "bold",
		textTransform: "uppercase",
		color: constants.BrandTeal,
	},
})
