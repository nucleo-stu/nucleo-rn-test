import React, { Component } from "react"
import { StyleSheet, Text, TextInput, TouchableOpacity, View } from "react-native"
import { TextBoxInput } from "./FormElements"

class ProfileForm extends Component {
	state = {
		formData: {
			firstName: "",
			lastName: "",
			email: "",
			mobile: "",
		},
	}

	handleUpdateField = (name) => (value) => {
		const { handleUpdateField } = this.props
		handleUpdateField(name, value)
	}

	render() {
		const { hasProfile } = this.props

		return (
			<View style={styles.container}>
				<Text style={styles.heading}>{!hasProfile ? "Please fill out" : "Edit"} your profile details:</Text>
				<View>
					<TextBoxInput handleSave={this.handleUpdateField("firstName")} placeholder={"First Name"} />
					<TextBoxInput handleSave={this.handleUpdateField("lastName")} placeholder={"Last Name"} />
					<TextBoxInput handleSave={this.handleUpdateField("email")} placeholder={"Email Address"} />
					<TextBoxInput handleSave={this.handleUpdateField("mobile")} placeholder={"Mobile Number"} />
				</View>
			</View>
		)
	}
}

export default ProfileForm

const styles = StyleSheet.create({
	container: {
		flex: 1,
		paddingTop: 30,
		paddingBottom: 400,
		paddingHorizontal: 30,
	},
	heading: {
		fontSize: 20,
		fontWeight: "400",
		paddingBottom: 15,
	},
	formField: {
		borderWidth: 1,
		borderColor: "#eee",
		borderStyle: "solid",
		paddingVertical: 5,
		paddingHorizontal: 5,
		marginVertical: 5,
	},
	buttonCreate: {
		marginVertical: 5,
		backgroundColor: "#1f8276",
		padding: 10,
	},
	buttonUpdate: {
		marginVertical: 5,
		backgroundColor: "#0B3A53",
		padding: 10,
	},
	buttonDelete: {
		marginVertical: 5,
		backgroundColor: "#FD3464",
		padding: 10,
	},
	buttonText: {
		color: "#fff",
		textAlign: "center",
	},
})
