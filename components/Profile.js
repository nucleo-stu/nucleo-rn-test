import React, { Component } from "react"
import { connect } from "react-redux"
import { createStackNavigator } from "@react-navigation/stack"
import { updateProfileField } from "../actions/profileActions"
import { ScrollView, StyleSheet, View, Text, SafeAreaView } from "react-native"
import ProfileDetails from "./ProfileDetails"
import ProfileForm from "./ProfileForm"
import { NavigationContainer } from "@react-navigation/native"

const Stack = createStackNavigator()

class Profile extends Component {
	componentDidMount() {
		this.loadProfile()
	}

	componentDidUpdate(prevProps, prevState, snapshot) {
		console.log(this.props.profile)
	}

	loadProfile = () => {
		const { loadProfile } = this.props
		loadProfile()
	}

	handleUpdateField = (name, value) => {
		const { updateProfileField } = this.props
		updateProfileField(name, value)
	}

	handleDelete = () => {
		const { deleteProfile } = this.props
		deleteProfile()
	}

	render() {
		const { profile, isLoaded, hasProfile } = this.props

		return (
			<View style={styles.container}>
				<SafeAreaView style={styles.container}>
					<ScrollView style={styles.scrollView}>
						{isLoaded && (
							<>
								<ProfileDetails profile={profile} />
								<Text>{JSON.stringify(this.props)}</Text>
								<ProfileForm
									hasProfile={hasProfile}
									handleUpdateField={this.handleUpdateField}
									handleDelete={this.handleDelete}
									profile={profile}
								/>
							</>
						)}
					</ScrollView>
				</SafeAreaView>
			</View>
		)
	}
}

function mapStateToProps(state) {
	return {
		profile: state.profile.data,
		isLoaded: state.profile.isLoaded,
		hasProfile: state.profile.hasProfile,
	}
}

export default connect(mapStateToProps, {
	updateProfileField,
})(Profile)

const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: "#fff",
	},
})
