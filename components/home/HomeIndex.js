import React, { Component } from "react"
import { Text, View, StyleSheet, TouchableOpacity } from "react-native"
import { connect } from "react-redux"
import { Wrapper, Container } from "../Layout"
import { HeadingTwo, Content, HeadingFour } from "../Typography"
import * as constants from "../../lib/constants"
import { Button, Card } from "react-native-paper"
import { MaterialIcons } from "@expo/vector-icons"

const setHomeNavIcon = (iconName) => (props) => {
	return (
		<View style={StyleSheet.flatten([props.style, styles.iconWrap])} {...props}>
			<MaterialIcons name={iconName} size={28} color={"#FFF"} />
		</View>
	)
}

const HomeScreenProfileInfo = ({ name }) => {
	return (
		<View>
			{name && (
				<HeadingTwo center rev>
					{name}
				</HeadingTwo>
			)}
		</View>
	)
}

class HomeIndex extends Component {
	render() {
		const { navigation, profileLoaded } = this.props

		return (
			<View style={{ flex: 1, flexDirection: "column" }}>
				<View style={styles.container}>
					<HeadingTwo center rev>
						Welcome!
					</HeadingTwo>
					{profileLoaded && <HomeScreenProfileInfo name={"John Doe"} />}
					<Content center rev>
						This app is designed to help people living with a mental disability through the NDIS process.
					</Content>
				</View>
				<View style={styles.navWrapper}>
					<TouchableOpacity
						activeOpacity={0.9}
						onPress={() => {
							navigation.navigate("Profile")
						}}
					>
						<Card.Title title={"My Info"} style={styles.profileLink} titleStyle={{ color: "#FFF" }} right={setHomeNavIcon("person")} />
					</TouchableOpacity>
					<TouchableOpacity
						activeOpacity={0.9}
						onPress={() => {
							navigation.navigate("Workbook")
						}}
					>
						<Card.Title
							title={"Continue My Workbook"}
							style={styles.workbookLink}
							titleStyle={{ color: "#FFF" }}
							right={setHomeNavIcon("view-list")}
						/>
					</TouchableOpacity>
				</View>
			</View>
		)
	}
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		alignItems: "center",
		justifyContent: "center",
		padding: 20,
		backgroundColor: constants.BrandOlive,
	},
	navWrapper: {},
	iconWrap: {
		paddingRight: 40,
	},
	profileLink: {
		backgroundColor: constants.BrandTeal,
	},
	workbookLink: {
		backgroundColor: constants.BrandPurpleLight,
	},
})

function mapStateToProps(state) {
	return {
		profileLoaded: state.profile.isLoaded,
	}
}

export default connect(mapStateToProps)(HomeIndex)
