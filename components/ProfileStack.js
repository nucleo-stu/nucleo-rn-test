import React, { Component } from "react"
import { createStackNavigator } from "@react-navigation/stack"
import PersonalDetails from "./profile/PersonalDetails"
import ProfileIndex from "./profile/ProfileIndex"

const Stack = createStackNavigator()

export const ProfileStack = () => {
	return (
		<Stack.Navigator initialRouteName={"Profile"}>
			<Stack.Screen name={"Profile"} component={ProfileIndex} />
			<Stack.Screen name={"Personal Details"} component={PersonalDetails} />
		</Stack.Navigator>
	)
}
