import React from 'react';
import { Button, View, StyleSheet,Image } from "react-native"


export const StepImage = ({source, ...props}) => {
    return (
        <View style={styles.imageWrap}>
            <Image style={styles.image} source={source} resizeMode={"contain"} {...props}/>
        </View>
    )
}

const styles = StyleSheet.create({
    imageWrap: {
        justifyContent: "center",
        paddingVertical: 30,
        flexDirection: "row",
    },
    image: {
        height: 250,
        width: 250
    }
})