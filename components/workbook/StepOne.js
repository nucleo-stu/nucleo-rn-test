import React, { Component, useState } from "react"
import { connect } from "react-redux"
import { View, Text, Image } from "react-native"
import { Card, List } from "react-native-paper"
import { Center, Container, Wrapper } from "../Layout"
import { Content, HeadingThree, TextLink } from "../Typography"
import { StepImage } from "./StepImage"
import * as constants from "../../lib/constants"
import { TextBoxInput } from "../FormElements"
import { updateStageField, loadStage } from "../../actions/workbookActions"
import WorkbookProgressBar from "../ProgressBar"

const imgSrc = require("../../assets/step-graphics/1.png")

const AccordionItem = ({ children }) => {
	const [expanded, setExpanded] = useState(false)

	return (
		<List.Accordion
			title="Controlled Accordion"
			style={{
				backgroundColor: constants.LightGrey,
				marginHorizontal: -20,
				paddingHorizontal: 15,
			}}
			titleStyle={{
				color: constants.TextColour,
				fontWeight: "bold",
				fontSize: 18,
			}}
			// left={props => <List.Icon {...props} icon="folder" />}
			expanded={expanded}
			onPress={() => setExpanded(!expanded)}
		>
			<View
				style={{
					marginHorizontal: -20,
					padding: 15,
				}}
			>
				{children}
			</View>
		</List.Accordion>
	)
}

const StepHeading = ({ title, subtitle, progress, textColour, bgColour, stageLoaded }) => {
	return (
		<Container style={{ backgroundColor: bgColour || "#fff" }}>
			<Card.Title title={title} subtitle={subtitle} titleStyle={{ color: textColour }} subtitleStyle={{ color: textColour }} />
			{stageLoaded && <WorkbookProgressBar progress={progress} />}
		</Container>
	)
}

class StepOne extends Component {
	componentDidMount() {
		const { loadStage } = this.props
		loadStage(1)
		console.log("Stage one mounted")
	}

	getProgressValue = () => {
		const { stageLoaded, fields } = this.props

		if (stageLoaded) {
			const completeCount = fields.filter((field) => field.complete !== 0).length

			console.log(completeCount)

			return completeCount / fields.length
		} else {
			return 0
		}
	}

	saveField = (key) => (value) => {
		const { updateStageField } = this.props
		updateStageField(key, value)
	}

	getInitialFieldValue = (key) => {
		const { fields } = this.props

		return fields.find((field) => field.fieldKey === key).content
	}

	render() {
		const { workbook, fields, stageLoaded, progress } = this.props

		return (
			<Wrapper>
				<StepHeading
					title={"Step 1"}
					subtitle={"Understand psychosocial disability"}
					textColour={"#fff"}
					bgColour={constants.BrandPurple}
					stageLoaded={stageLoaded}
					progress={progress}
				/>
				{/*<Text>{JSON.stringify(fields)}</Text>*/}
				<Container>
					<Content>
						This section is to help you start gathering the information you may need for your NDIS application (called an access request). You can
						use this information to guide your health and community service workers when they are helping you put together your evidence.
					</Content>

					<StepImage source={imgSrc} />
					<HeadingThree>My support needs </HeadingThree>
					<Content>
						If you are going to apply for the NDIS you will be asked to provide information about your psychosocial and any other disability support
						needs.
					</Content>
					<Content>If you do not understand what psychosocial disability is - check out Step 1 of the reimagine.today website.</Content>
					<TextLink href={"https://google.com"}>Step 1 of the reimagine.today</TextLink>

					<AccordionItem>
						{stageLoaded ? (
							<View>
								<View>
									<Center>
										<HeadingThree center>Mobility or motor skills</HeadingThree>
										<Content center>Getting out of bed and moving around</Content>
									</Center>
									<TextBoxInput
										placeholder={"Fill in your answer here"}
										initialValue={this.getInitialFieldValue("s1_mobms")}
										handleSave={this.saveField("s1_mobms")}
									/>
								</View>
								<View>
									<Center>
										<HeadingThree center>Mobility or motor skills</HeadingThree>
										<Content center>Getting out of bed and moving around</Content>
									</Center>
									<TextBoxInput
										placeholder={"Fill in your answer here"}
										initialValue={this.getInitialFieldValue("s1_comm")}
										handleSave={this.saveField("s1_comm")}
									/>
								</View>
								<View>
									<Center>
										<HeadingThree center>Mobility or motor skills</HeadingThree>
										<Content center>Getting out of bed and moving around</Content>
									</Center>
									<TextBoxInput
										placeholder={"Fill in your answer here"}
										initialValue={this.getInitialFieldValue("s1_socin")}
										handleSave={this.saveField("s1_socin")}
									/>
								</View>
								<View>
									<Center>
										<HeadingThree center>Mobility or motor skills</HeadingThree>
										<Content center>Getting out of bed and moving around</Content>
									</Center>
									<TextBoxInput
										placeholder={"Fill in your answer here"}
										initialValue={this.getInitialFieldValue("s1_learn")}
										handleSave={this.saveField("s1_learn")}
									/>
								</View>
								<View>
									<Center>
										<HeadingThree center>Mobility or motor skills</HeadingThree>
										<Content center>Getting out of bed and moving around</Content>
									</Center>
									<TextBoxInput
										placeholder={"Fill in your answer here"}
										initialValue={this.getInitialFieldValue("s1_selfcare")}
										handleSave={this.saveField("s1_selfcare")}
									/>
								</View>
								<View>
									<Center>
										<HeadingThree center>Mobility or motor skills</HeadingThree>
										<Content center>Getting out of bed and moving around</Content>
									</Center>
									<TextBoxInput
										placeholder={"Fill in your answer here"}
										initialValue={this.getInitialFieldValue("s1_selfmanag")}
										handleSave={this.saveField("s1_selfmanag")}
									/>
								</View>
							</View>
						) : (
							<Text>Loading...</Text>
						)}
					</AccordionItem>
				</Container>
			</Wrapper>
		)
	}
}

function mapStateToProps(state) {
	return {
		workbook: state.workbook,
		fields: state.workbook.data.currentStageFields,
		progress: state.workbook.data.currentStageProgress,
		stageLoaded: state.workbook.isLoaded,
	}
}

export default connect(mapStateToProps, {
	updateStageField,
	loadStage,
})(StepOne)
