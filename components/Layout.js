import React from "react"
import { Button, View, StyleSheet, SafeAreaView, ScrollView } from "react-native"

export const Container = ({ children, ...props }) => {
	const setStyles = () => {
		if (props.style) {
			return StyleSheet.compose(styles.container, props.style)
		} else {
			return styles.container
		}
	}

	return (
		<View style={setStyles()} {...props}>
			{children}
		</View>
	)
}

export const Wrapper = ({ children, ...props }) => {
	return (
		<SafeAreaView style={styles.wrapper}>
			<ScrollView style={{ flex: 1 }}>{children}</ScrollView>
		</SafeAreaView>
	)
}

export const Center = ({ children }) => {
	return <View style={styles.center}>{children}</View>
}

const styles = StyleSheet.create({
	wrapper: {
		flex: 1,
		backgroundColor: "#fff",
	},
	container: {
		flex: 1,
		padding: 20,
		backgroundColor: "#fff",
	},
	center: {
		flex: 1,
		justifyContent: "center",
		textAlign: "center",
	},
})
