import * as React from "react"
import { ProgressBar, Colors } from "react-native-paper"
import { View, StyleSheet } from "react-native"
import * as constants from "../lib/constants"

const WorkbookProgressBar = ({ progress }) => (
	<View style={styles.wrapper}>
		<ProgressBar progress={progress} color={constants.BrandOlive} />
	</View>
)

const styles = StyleSheet.create({
	wrapper: {
		padding: 20,
	},
})

export default WorkbookProgressBar
