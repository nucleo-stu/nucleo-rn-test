import "react-native-gesture-handler"
import React, { Component } from "react"
import { NavigationContainer } from "@react-navigation/native"
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs"
import { Provider as StoreProvider, connect } from "react-redux"
import { Provider as PaperProvider } from "react-native-paper"
import { SafeAreaView, ScrollView, StyleSheet, View, Text } from "react-native"
import Profile from "./components/Profile"
import store from "./lib/store"
import Database from "./lib/db"
import { Ionicons } from "@expo/vector-icons"
import { ProfileStack } from "./components/ProfileStack"
import { WorkbookStack } from "./components/WorkbookStack"

import { createStackNavigator } from "@react-navigation/stack"
import ProfileIndex from "./components/profile/ProfileIndex"
import PersonalDetails from "./components/profile/PersonalDetails"
import InitScreen from "./components/InitScreen"
import HomeIndex from "./components/home/HomeIndex"
import * as constants from "./lib/constants"

const MainStack = () => {
	const Tab = createBottomTabNavigator()

	return (
		<Tab.Navigator
			screenOptions={({ route }) => ({
				tabBarPosition: "bottom",
				tabBarIcon: ({ focused, color, size }) => {
					let iconName

					if (route.name === "Profile") {
						iconName = focused ? "ios-information-circle" : "ios-information-circle-outline"
					} else if (route.name === "Workbook") {
						iconName = focused ? "ios-list-box" : "ios-list"
					} else if (route.name === "Home") {
						iconName = focused ? "md-home" : "md-home"
					}

					// You can return any component that you like here!
					return (
						<View style={styles.tabBarIconWrap}>
							{focused && <View style={styles.tabBarIconWrapActiveShadow}></View>}

							<Ionicons name={iconName} size={size} style={styles.tabNavIcon} color={color} />
						</View>
					)
				},
			})}
			tabBarOptions={{
				activeTintColor: "#fff",
				inactiveTintColor: "#fff",
				showLabel: false,
				tabStyle: {
					paddingVertical: 10,
				},
				style: {
					backgroundColor: constants.BrandPurple,
					height: 60,
				},
			}}
		>
			<Tab.Screen name="Home" component={HomeIndex} />
			<Tab.Screen name="Profile" component={ProfileStack} />
			<Tab.Screen name="Workbook" component={WorkbookStack} />
		</Tab.Navigator>
	)
}

export const InitStack = () => {
	const Stack = createStackNavigator()

	return (
		<Stack.Navigator
			initialRouteName={"Init"}
			screenOptions={{
				headerShown: false,
			}}
		>
			<Stack.Screen name={"Init"} component={InitScreen} />
			<Stack.Screen name={"Home"} component={MainStack} />
		</Stack.Navigator>
	)
}

class App extends Component {
	state = {
		initialisedDatabase: false,
	}

	componentDidMount() {
		Database.createTables()
			.then(Database.runMigrations)
			.then(() => {
				this.setState({
					initialisedDatabase: true,
				})
			})
	}

	render() {
		return (
			<StoreProvider store={store}>
				<PaperProvider>
					<NavigationContainer>
						<InitStack />
					</NavigationContainer>
				</PaperProvider>
			</StoreProvider>
		)
	}
}

export default App

const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: "#fff",
	},
	tabBarIconWrap: { width: 40, height: 40, textAlign: "center", position: "relative", alignItems: "center", justifyContent: "center" },
	tabBarIconWrapActiveShadow: {
		position: "absolute",
		top: 0,
		left: 0,
		width: "100%",
		height: "100%",
		borderRadius: 20,
		backgroundColor: "#fff",
		opacity: 0.2,
	},
	tabNavIcon: {
		zIndex: 1,
	},
})
